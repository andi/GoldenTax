﻿namespace GoldenTax
{
    partial class FormImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.FormSpliter = new System.Windows.Forms.SplitContainer();
        	this.buttonReWrite = new System.Windows.Forms.Button();
        	this.buttonLoad = new System.Windows.Forms.Button();
        	this.buttonBrowser = new System.Windows.Forms.Button();
        	this.textBoxPath = new System.Windows.Forms.TextBox();
        	this.dataGridViewInvoice = new System.Windows.Forms.DataGridView();
        	((System.ComponentModel.ISupportInitialize)(this.FormSpliter)).BeginInit();
        	this.FormSpliter.Panel1.SuspendLayout();
        	this.FormSpliter.Panel2.SuspendLayout();
        	this.FormSpliter.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridViewInvoice)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// FormSpliter
        	// 
        	this.FormSpliter.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.FormSpliter.IsSplitterFixed = true;
        	this.FormSpliter.Location = new System.Drawing.Point(0, 0);
        	this.FormSpliter.Name = "FormSpliter";
        	this.FormSpliter.Orientation = System.Windows.Forms.Orientation.Horizontal;
        	// 
        	// FormSpliter.Panel1
        	// 
        	this.FormSpliter.Panel1.Controls.Add(this.buttonReWrite);
        	this.FormSpliter.Panel1.Controls.Add(this.buttonLoad);
        	this.FormSpliter.Panel1.Controls.Add(this.buttonBrowser);
        	this.FormSpliter.Panel1.Controls.Add(this.textBoxPath);
        	// 
        	// FormSpliter.Panel2
        	// 
        	this.FormSpliter.Panel2.Controls.Add(this.dataGridViewInvoice);
        	this.FormSpliter.Size = new System.Drawing.Size(784, 562);
        	this.FormSpliter.SplitterDistance = 83;
        	this.FormSpliter.TabIndex = 0;
        	// 
        	// buttonReWrite
        	// 
        	this.buttonReWrite.Location = new System.Drawing.Point(673, 31);
        	this.buttonReWrite.Name = "buttonReWrite";
        	this.buttonReWrite.Size = new System.Drawing.Size(75, 23);
        	this.buttonReWrite.TabIndex = 4;
        	this.buttonReWrite.Text = "回写导入";
        	this.buttonReWrite.UseVisualStyleBackColor = true;
        	this.buttonReWrite.Click += new System.EventHandler(this.buttonReWrite_Click);
        	// 
        	// buttonLoad
        	// 
        	this.buttonLoad.Location = new System.Drawing.Point(592, 31);
        	this.buttonLoad.Name = "buttonLoad";
        	this.buttonLoad.Size = new System.Drawing.Size(75, 23);
        	this.buttonLoad.TabIndex = 3;
        	this.buttonLoad.Text = "加载";
        	this.buttonLoad.UseVisualStyleBackColor = true;
        	this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
        	// 
        	// buttonBrowser
        	// 
        	this.buttonBrowser.Location = new System.Drawing.Point(28, 29);
        	this.buttonBrowser.Name = "buttonBrowser";
        	this.buttonBrowser.Size = new System.Drawing.Size(75, 23);
        	this.buttonBrowser.TabIndex = 1;
        	this.buttonBrowser.Text = "选择文件";
        	this.buttonBrowser.UseVisualStyleBackColor = true;
        	this.buttonBrowser.Click += new System.EventHandler(this.buttonBrowser_Click);
        	// 
        	// textBoxPath
        	// 
        	this.textBoxPath.BackColor = System.Drawing.SystemColors.ControlLightLight;
        	this.textBoxPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.textBoxPath.ForeColor = System.Drawing.Color.Red;
        	this.textBoxPath.Location = new System.Drawing.Point(124, 31);
        	this.textBoxPath.Name = "textBoxPath";
        	this.textBoxPath.ReadOnly = true;
        	this.textBoxPath.Size = new System.Drawing.Size(452, 21);
        	this.textBoxPath.TabIndex = 2;
        	this.textBoxPath.WordWrap = false;
        	this.textBoxPath.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxPathKeyDown);
        	// 
        	// dataGridViewInvoice
        	// 
        	this.dataGridViewInvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.dataGridViewInvoice.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.dataGridViewInvoice.Location = new System.Drawing.Point(0, 0);
        	this.dataGridViewInvoice.Name = "dataGridViewInvoice";
        	this.dataGridViewInvoice.RowTemplate.Height = 23;
        	this.dataGridViewInvoice.Size = new System.Drawing.Size(784, 475);
        	this.dataGridViewInvoice.TabIndex = 0;
        	// 
        	// FormImport
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(784, 562);
        	this.Controls.Add(this.FormSpliter);
        	this.Name = "FormImport";
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "金税发票回写";
        	this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        	this.FormSpliter.Panel1.ResumeLayout(false);
        	this.FormSpliter.Panel1.PerformLayout();
        	this.FormSpliter.Panel2.ResumeLayout(false);
        	((System.ComponentModel.ISupportInitialize)(this.FormSpliter)).EndInit();
        	this.FormSpliter.ResumeLayout(false);
        	((System.ComponentModel.ISupportInitialize)(this.dataGridViewInvoice)).EndInit();
        	this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer FormSpliter;
        private System.Windows.Forms.Button buttonBrowser;
        private System.Windows.Forms.TextBox textBoxPath;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Button buttonReWrite;
        private System.Windows.Forms.DataGridView dataGridViewInvoice;
    }
}