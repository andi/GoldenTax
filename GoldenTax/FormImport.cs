﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ParseLib;
using System.Configuration;
using System.Data.Linq;

namespace GoldenTax
{
    public partial class FormImport : Form
    {
        public FormImport()
        {
            InitializeComponent();
        }

        private string conn = ConfigurationManager.ConnectionStrings["KDDBConnection"].ToString();
        private OpenFileDialog fileDialog = new OpenFileDialog();
        private IList<InvoiceHeader> headers= new List<InvoiceHeader>();
        

        private void buttonBrowser_Click(object sender, EventArgs e)
        {
            fileDialog.Multiselect = false;
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                textBoxPath.Text = "";
                textBoxPath.ForeColor = Color.Black;
                for (int i = 0; i < fileDialog.FileNames.Length; i++)
                {
                    textBoxPath.Text = textBoxPath.Text + fileDialog.FileNames.GetValue(i).ToString() + Environment.NewLine;
                }
                buttonLoad.Focus();
            }
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            if (File.Exists(fileDialog.FileName.ToString()))
            {
                try
                {
                    headers = Parser.GetInvoiceHeader(fileDialog.FileName.ToString());

                    if (headers.Any())
                    {
                    	dataGridViewInvoice.DataSource=headers;
                    	buttonReWrite.Focus();
                    }
                    else
                    {
                        MessageBox.Show(@"无数据或文件未能正确解析！");
                        buttonBrowser.Focus();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    buttonBrowser.Focus();
                    throw;
                }
                
            }
            else
            {
                MessageBox.Show("请选择正确文件！");
                buttonBrowser.Focus();
            }

        }

        private void buttonReWrite_Click(object sender, EventArgs e)
        {
            if (headers.Any())
            {
                using (var dbContext = new DataContext(conn))
                {
                    foreach (InvoiceHeader header in headers)
                    {
                        string queryStr = string.Format("EXEC UP_UpdateInvoiceNumber '{0}','{1}'", header.FViutual,
                            header.FInvoiceCode +"_"+ header.FInvoiceNumber);
                        dbContext.ExecuteCommand(queryStr);
                    }
                    dbContext.SubmitChanges();
                    MessageBox.Show("更新完成！");
                }
            }
            else
            {
                MessageBox.Show("无数据需要更新！");
            }
        }
		void TextBoxPathKeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyCode==Keys.Enter)
				buttonLoad_Click(sender,e);
		}
    }
}
