﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GoldenTax
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private int count = 0;

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxLoginid.Text) || string.IsNullOrEmpty(textBoxPswd.Text))
            {
                count += 1;
                MessageBox.Show(string.Format(@"用户名、密码不能为空！您还有{0}次机会。", 3 - count));
                if (count == 3)
                    Application.Exit();
            }
            else
            {
                if ("admin"==textBoxLoginid.Text&&"admin"==textBoxPswd.Text) //此处偷懒了
                {
                	DialogResult = DialogResult.OK;
                }
                else
                {
                    count += 1;
                    MessageBox.Show(string.Format("用户名或密码错误！您还有{0}次机会。", 3 - count));
                    if (count == 3)
                        Application.Exit();
                }
            }
        }

        private void textBoxLoginid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == (int)Keys.Enter)
                SendKeys.Send("{Tab}");
        }

        private void textBoxPswd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                buttonLogin_Click(sender, e);
        }
    }
}
