﻿/*
 * 由SharpDevelop创建。
 * 用户： Administrator
 * 日期: 2015/3/16
 * 时间: 22:17
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Data.Linq.Mapping;

namespace GoldenTax
{
    [Table(Name = "VW_T_Invoice")]
    public class K3Invoice
    {
        /// <summary>
        /// ID
        /// </summary>
        [Column(Name = "FDetailId", IsPrimaryKey = true, IsDbGenerated = true, DbType = "Int")]
        public int FDetailId { get; set; }

        /// <summary>
        /// 红蓝字
        /// </summary>
        [Column(Name = "Frob", DbType = "Varchar(4)")]
        public string Frob { get; set; }

        /// <summary>
        /// 单据号码
        /// </summary>
        [Column(Name = "FInvoiceNumber", DbType = "Varchar(512)")]
        public string FInvoiceNumber { get; set; }

        /// <summary>
        /// 购方名称
        /// </summary>
        [Column(Name = "FCustName", DbType = "Varchar(256)")]
        public string FCustName { get; set; }

        /// <summary>
        /// 购方税号
        /// </summary>
        [Column(Name = "FDuty", DbType = "Varchar(50)")]
        public string FDuty { get; set; }

        /// <summary>
        /// 购方地址电话
        /// </summary>
        [Column(Name = "FAddres", DbType = "Varchar(640)")]
        public string FAddres { get; set; }

        /// <summary>
        /// 购方银行账号
        /// </summary>
        [Column(Name = "FBank", DbType = "Varchar(640)")]
        public string FBank { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Column(Name = "FNotes", DbType = "Varchar(640)")]
        public string FNotes { get; set; }

        /// <summary>
        /// 复核人
        /// </summary>
        [Column(Name = "FChecker", DbType = "Varchar(16)")]
        public string FChecker { get; set; }

        /// <summary>
        /// 收款人
        /// </summary>
        [Column(Name = "FReceiver", DbType = "Varchar(16)")]
        public string FReceiver { get; set; }

        /// <summary>
        /// 清单商品名称
        /// </summary>
        [Column(Name = "FProductName", DbType = "Varchar(128)")]
        public string FProductName { get; set; }

        /// <summary>
        /// 单据日期
        /// </summary>
        [Column(Name = "FDate", DbType = "DateTime")]
        public DateTime? FDate { get; set; }

        /// <summary>
        /// 销方银行账号
        /// </summary>
        [Column(Name = "FComBank", DbType = "Varchar(64)")]
        public string FComBank { get; set; }

        /// <summary>
        /// 销方地址电话
        /// </summary>
        [Column(Name = "FComAddress", DbType = "Varchar(128)")]
        public string FComAddress { get; set; }

        /// <summary>
        /// 身份证校验标志
        /// </summary>
        [Column(Name = "IdTag", DbType = "Int")]
        public int? IdTag { get; set; }

        /// <summary>
        /// 海洋石油标志
        /// </summary>
        [Column(Name = "SeaOilTag", DbType = "Int")]
        public int? SeaOilTag { get; set; }

        /// <summary>
        /// 货物名称
        /// </summary>
        [Column(Name = "FItemName", DbType = "Varchar(128)")]
        public string FItemName { get; set; }

        /// <summary>
        /// 计量单位
        /// </summary>
        [Column(Name = "FUnitName", DbType = "Varchar(256)")]
        public string FUnitName { get; set; }

        /// <summary>
        /// 规格型号
        /// </summary>
        [Column(Name = "FModel", DbType = "Varchar(256)")]
        public string FModel { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Column(Name = "FQty", DbType = "Decimal(20)")]
        public decimal? FQty { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        [Column(Name = "FAmount", DbType = "Decimal(20)")]
        public decimal? FAmount { get; set; }

        /// <summary>
        /// 税率
        /// </summary>
        [Column(Name = "FTaxRate", DbType = "Decimal(20)")]
        public decimal? FTaxRate { get; set; }

        /// <summary>
        /// 商品税目
        /// </summary>
        [Column(Name = "TaxItems", DbType = "int")]
        public int? TaxItems { get; set; }

        /// <summary>
        /// 折扣金额
        /// </summary>
        [Column(Name = "FAmtDiscount", DbType = "Decimal(20)")]
        public decimal? FAmtDiscount { get; set; }

        /// <summary>
        /// 税额
        /// </summary>
        [Column(Name = "FTaxAmount", DbType = "Decimal(20)")]
        public decimal? FTaxAmount { get; set; }

        /// <summary>
        /// 折扣税额
        /// </summary>
        [Column(Name = "FDiscountTaxAmount", DbType = "Decimal(20)")]
        public decimal? FDiscountTaxAmount { get; set; }

        /// <summary>
        /// 折扣率
        /// </summary>
        [Column(Name = "FDiscountRate", DbType = "Decimal(20)")]
        public decimal? FDiscountRate { get; set; }

        /// <summary>
        /// 单价
        /// </summary>
        [Column(Name = "FPrice", DbType = "Decimal(20))")]
        public decimal? FPrice { get; set; }

        /// <summary>
        /// 价格方式
        /// </summary>
        [Column(Name = "PriceOffer", DbType = "int")]
        public int? PriceOffer { get; set; }
    }
}